from django.urls import path
from emni.views import home

urlpatterns = [
    path('', home),
]
